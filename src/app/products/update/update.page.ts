import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ProductService } from "../product.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { productos } from '../products.model';

@Component({
  selector: 'app-update',
  templateUrl: './update.page.html',
  styleUrls: ['./update.page.scss'],
})
export class UpdatePage implements OnInit {
  product: productos;
  formProductUpdate: FormGroup;

  constructor(
    private activeRouter: ActivatedRoute,
    private serviceProduct: ProductService,
    private router: Router
  ) { }

  ngOnInit() {
    this.activeRouter.paramMap.subscribe(paramMap => {
      if (!paramMap.has('productId')) {
        return;
      }
      const productId = parseInt(paramMap.get('productId'));
      this.product = this.serviceProduct.getProduct(productId);
    });

    this.formProductUpdate = new FormGroup({
    
      
      pnombre: new FormControl(this.product.nombre,
        {
          updateOn: 'blur',
          validators:[Validators.required, Validators.minLength(3)]
        }
      ),
      pcodigo: new FormControl(this.product.codigo,
        {
          updateOn: 'blur',
          validators:[Validators.required, Validators.minLength(3)]
        }
      ),
      pmarcador: new FormControl(this.product.marcador,
        {
          updateOn: 'blur',
          validators:[Validators.required, Validators.minLength(5)]
        }
      ),
      
      pdescripcion: new FormControl(this.product.descripcion,
        {
          updateOn: 'blur',
          validators:[Validators.required, Validators.minLength(30)]
        }
      )
    });
    this.formProductUpdate.value.pnombre = this.product.nombre;

  }

  updateProduct(){
    if(!this.formProductUpdate.valid){
      return;
    }
    this.serviceProduct.UpdateProduct(
      this.formProductUpdate.value.pnombre,
      this.formProductUpdate.value.pcodigo,
      this.formProductUpdate.value.pmarcador, 
      this.formProductUpdate.value.pdescripcion
    );
    this.formProductUpdate.reset();
    this.router.navigate(['./products']);
  }  
}
