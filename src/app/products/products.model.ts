
export interface productos{
    codigo: number;
    nombre: string;
    marcador: string;
    fecha_partido: string;
    descripcion: string;
    proveedor: proveedor[];
}
export interface proveedor{
    nombre: string;
    codigo: number;
}
