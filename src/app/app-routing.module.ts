import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'products',
    loadChildren: () => import('./products/products.module').then( m => m.ProductsPageModule)
  },
  {
    path: '',
    redirectTo: 'products',
    pathMatch: 'full'
  },
  {
    path: 'products',
    children:[
      {
        path: '',
        loadChildren: './products/products.module#ProductsPageModule'
      },
      {
        path: 'add',
        loadChildren: './products/add/add.module#AddPageModule' 
      },
      {
        path: ':productId',
        loadChildren: './products/detail/detail.module#DetailPageModule'
      },
      {
        path: "update/:productId",
        loadChildren: "./products/update/update.module#UpdatePageModule",
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
